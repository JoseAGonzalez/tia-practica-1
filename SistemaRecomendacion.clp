;; Clases y extensiones ;;
(deftemplate Compañia
    (slot idCompañia (type symbol))
    (slot nombre (type string))
    (slot director (type string))
    (multislot cadenasCompañia (type symbol))
)

(deftemplate Cadena
    (slot idCadena    (type symbol))
    (slot nombre      (type string))
    (slot online      (type symbol))
    (slot rutaDirecto (type string))
    (slot alcance     (type symbol))
    (multislot programasCadena (type symbol))
)

(deftemplate CadenaTV extends Cadena
    (slot pago (type symbol))
    (slot tdt  (type symbol))
)

(deftemplate CadenaRadio extends Cadena
    (slot frecuencia (type symbol))
)

; Tipo programa: Serie,Película,Programa ;
; Tematica programa: Infantil,Ciencia,Tecnología,Entretenimiento,Debate,Música,Aventuras,Noticias,Acción,Comedia,Animadas,Fantasía,Adultos,Economía,Política,Drama,Gastronomía,Corazón,Época,Bélica,Documental,Romántico,Deporte,Adolescente, Juego;
(deftemplate Programa
    (slot idPrograma (type string))
    (slot nombre   (type string))
	(slot tipo     (type symbol) (allowed-values Serie Pelicula Programa))
    (slot horario  (type integer))
    (slot edadRecomendada (type integer))
    (slot sinopsis (type string))
    (slot tematica (type symbol))
    (slot explorado (type symbol))
    (multislot empleadosPrograma (type string))
)

(deftemplate Persona
    (slot nombre    (type string))
	(slot edad      (type integer))
    (slot pais      (type symbol))
    (slot cautonoma (type symbol))   
    (slot edad		(type integer)) 
    (slot estadoCivil  (type symbol) (allowed-values SOLTERO CASADO))
    (slot hijos		(type symbol) (allowed-values SI NO))
    (slot trabajo	(type symbol))
    (multislot hobbies   (type symbol))
    (slot personalidad (type symbol))
)

(deftemplate Usuario extends Persona
    (slot idUsuario			      (type symbol))
    (slot horarioVision			  (type integer))
    (slot tiempoVision			  (type integer))
    (slot disponiblePago    (type symbol))
    (multislot dispositivos (type symbol) (allowed-values SI NO)) ; (ONLINE,TV,RADIO)
    (multislot preferencias (type symbol))			; A determinar por el sistema ;
    (multislot programasRecomendados (type symbol)) ; A recomendar por el sistema ;
)

;; Instancias ;;
;; Usuario ;;
;(deffacts Usuarios
	;(Usuario (idUsuario Aldool) (nombre "Alberto Donet Olmeda") (edad 20) (estadoCivil SOLTERO) (hijos SI) (trabajo HOSTELERIA) (hobbies VIDEOJUEGOS) (pais España) (cautonoma Valencia) (horarioVision 1020) (tiempoVision 180) (dispositivos SI SI SI) (disponiblePago SI))
    ;(Usuario (idUsuario Jogonba) (nombre "José Ángel González Barba") (edad 21) (estadoCivil CASADO) (hijos SI) (trabajo EDUCACION) (hobbies DEPORTE MUSICA) (pais España) (cautonoma Valencia)  (horarioVision 930) (tiempoVision 180) (dispositivos SI SI SI) (disponiblePago SI))
   
;)
;; Compañias ;;
(deffacts Compañias
    (Compañia (idCompañia Mediaset)   (nombre "Mediaset")   (director "Paolo Vasile")   				(cadenasCompañia Telecinco Cuatro FDF Divinity COPE))
    (Compañia (idCompañia ATresMedia) (nombre "AtresMedia") (director "Javier Bardají")					(cadenasCompañia Antena3 LaSexta Neox Nova Mega SER))
    (Compañia (idCompañia RTVE)       (nombre "Grupo RTVE") (director "José Antonio Sánchez Domínguez") (cadenasCompañia TVE La2 Clan 24H RNE))
    (Compañia (idCompañia Telefonica) (nombre "Telefonica") (director "César Alierta")					(cadenasCompañia Canal+))
    (Compañia (idCompañia TBS)        (nombre "Turner Broadcasting System") (director "Time Warner")	(cadenasCompañia CartoonN))
    
)
;; Cadenas Television ;;
(deffacts CadenasTV
    (CadenaTV (idCadena Telecinco) (nombre "Telecinco") 	 (online SI) (rutaDirecto "http://mitele.es") 								 (alcance Nacional) (programasCadena Noticias5_mañana GranHermano AnaRosa PremierCasino FusionSonora MujeresyHombres Noticias5 Salvame Pasapalabra Noticias5_noche Bb) (pago NO) (tdt SI))
    (CadenaTV (idCadena Antena3)   (nombre "Antena 3")   	 (online SI) (rutaDirecto "http://www.atresplayer.com/directos/television/") (alcance Nacional) (programasCadena Noticias3 RinconPensar Poker GaningCasino Noticias3_mañana  Arguiñano RuletaSuerte Simpsons PuenteViejo AmarSiempre AhoraCaigo Noticias3_noche Peliculon Hormiguero) (pago NO) (tdt SI))
	(CadenaTV (idCadena TVE)	   (nombre "TVE")     	     (online SI) (rutaDirecto "http://www.rtve.es/directo/la-1/")     			 (alcance Nacional) (programasCadena Noticias1 Cine1 24h TVEmusica Noticias1_mañana Mañanadela1 TorresCocina Corazon Acacias38 SeisHermanas AquiTierra Noticias1_noche ComandoActualidad Cine1_otro) (pago NO) (tdt SI))  
    (CadenaTV (idCadena La2)	   (nombre "La2")       	 (online NO) (rutaDirecto "http://www.rtve.es/directo/la-2/")    			 (alcance Nacional) (programasCadena HierroyCromo TVE2Musica Bubbles English Agrosfera Paraisos Cine2 ViajeCentro SaberGanar Documentales Documental2 Cine2_otro) (pago NO) (tdt SI))  
    ;(CadenaTV (idCadena Tdp)	   (nombre "Teledeporte")    (online SI) (rutaDirecto "http://www.rtve.es/directo/teledeporte/")     	 (alcance Nacional) (programasCadena ) (pago NO) (tdt SI))
    (CadenaTV (idCadena LaSexta)   (nombre "LaSexta")    	 (online SI) (rutaDirecto "http://www.atresplayer.com/directos/television/") (alcance Nacional) (programasCadena PoliciasAccion PokerAdventure Juega8 MinMusicales CrimenImperfectos RojoVivo Noticias6 Jugones Zapeando MasValeTarde Noticias6_noche) (pago NO) (tdt SI))
    (CadenaTV (idCadena Cuatro)    (nombre "Cuatro") 	     (online SI) (rutaDirecto "http://www.cuatro.com/en-directo/") 				 (alcance Nacional) (programasCadena Cine4 PuroCuatro Zapping Billy AlertaCobra MañanasCuatro Noticias4 Deportes4 Hawai ReglasJuego Noticias4_noche GymTony CSI) (pago NO) (tdt SI))
    ;(CadenaTV (idCadena Neox)      (nombre "Neox") 		 (online NO) (rutaDirecto "http://www.atresplayer.com/directos/television/") (alcance Nacional) (programasCadena ) (pago NO) (tdt SI))
    ;(CadenaTV (idCadena Energy)    (nombre "Energy") 		 (online NO) (rutaDirecto "http://www.telecinco.es/energy/") 				 (alcance Nacional) (programasCadena ) (pago NO) (tdt SI))
    (CadenaTV (idCadena Canal+)    (nombre "Canal+") 		 (online NO) (rutaDirecto "http://www.plus.es/") 				 			 (alcance Nacional) (programasCadena GuerraMundial2 GuerraMundial Champions1 Champions2 CineAdultos1 CineAdultos2 Basket) (pago SI) (tdt NO))
    (CadenaTV (idCadena CartoonN)  (nombre "CartoonNetwork") (online NO) (rutaDirecto "http://www.cartoonnetwork.es/") 				 	 (alcance Nacional) (programasCadena Dibujos1 Dibujos2 Dibujos3 Dibujos4 Dibujos5 Dibujos6 Dibujos7 Dibujos8) (pago SI) (tdt NO))
)
;; Cadenas Radio ;;
(deffacts CadenasRadio
	(CadenaRadio (idCadena COPE) (nombre "COPE") (online SI) (rutaDirecto "http://lacope.es")  (alcance Nacional) (programasCadena ElLarguero) (frecuencia 102:8))
    (CadenaRadio (idCadena SER)  (nombre "Cadena SER")  (online SI) (rutaDirecto "http://laser.es") (alcance Nacional) (programasCadena ASaber2) (frecuencia 104:4))
	(CadenaRadio (idCadena RNE)	 (nombre "Radio Nacional De España")       (online SI) (rutaDirecto "http://rne.es")     (alcance Nacional) (programasCadena ElTecno) (frecuencia 97:7))
)
;; ProgramasTV ;;
(deffacts ProgramasTV
    ;El horario de los programas está en minutos.
    ;Tele5
	(Programa (idPrograma Noticias5_mañana) 		(nombre "Informativos Telecinco mañanas") 	(tipo Programa) (horario 0390) (edadRecomendada 7)  (sinopsis "Actualidad, noticias Telecinco") 		 (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma GranHermano) 				(nombre "Gran Hermano 16") 					(tipo Programa) (horario 0030) (edadRecomendada 18) (sinopsis "Payasos") 								 (tematica Drama)	 		(explorado NO))
    (Programa (idPrograma AnaRosa) 					(nombre "El programa de Ana Rosa") 			(tipo Programa) (horario 0540) (edadRecomendada 13) (sinopsis "Actualidad, entrevistas, cotilleos")  	 (tematica Corazon) 		(explorado NO))
    (Programa (idPrograma PremierCasino) 			(nombre "Premier Casino") 					(tipo Programa) (horario 0165) (edadRecomendada 18) (sinopsis "Juegos de casino")  						 (tematica Juego) (explorado NO))
    (Programa (idPrograma FusionSonora)				(nombre "Fusion Sonora") 					(tipo Programa) (horario 0300) (edadRecomendada 13) (sinopsis "Música, videoclips")  					 (tematica Musica) 			(explorado NO))
    (Programa (idPrograma MujeresyHombres) 			(nombre "Mujeres y Hombres y Viceversa") 	(tipo Programa) (horario 0765) (edadRecomendada 18) (sinopsis "Chonis, tetes y payasos") 				 (tematica Adolescente) 	(explorado NO))
    (Programa (idPrograma Noticias5) 				(nombre "Informativos Telecinco") 			(tipo Programa) (horario 0840) (edadRecomendada 7)  (sinopsis "Actualidad, noticias Telecinco")  		 (empleadosPrograma "Pedro Piqueras") (tematica Noticias) (explorado NO))
    (Programa (idPrograma Salvame) 					(nombre "Sálvame") 							(tipo Programa) (horario 0960) (edadRecomendada 13) (sinopsis "Cotilleo")  								 (tematica Corazon) 		(explorado NO))
    (Programa (idPrograma Pasapalabra) 				(nombre "Pasapalabra") 						(tipo Programa) (horario 1200) (edadRecomendada 7)  (sinopsis "Concurso")  								 (tematica Entretenimiento) (explorado NO))
    (Programa (idPrograma Noticias5_noche) 			(nombre "Informativos Telecinco noche") 	(tipo Programa) (horario 1260) (edadRecomendada 7)  (sinopsis "Actualidad, noticias Telecinco") 		 (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma Bb) 						(nombre "B&b") 								(tipo Serie) 	(horario 1360) (edadRecomendada 13) (sinopsis "Serie, comedia")  						 (tematica Comedia)  		(explorado NO))
    ;Antena3
    (Programa (idPrograma Noticias3) 				(nombre "Antena3 Noticias") 				(tipo Programa) (horario 0930) (edadRecomendada 7)  (sinopsis "Actualidad, noticias Antena3")  			 (empleadosPrograma "Matias Prats") (tematica Noticias) (explorado NO))
    (Programa (idPrograma RinconPensar) 			(nombre "Al rincón de pensar") 				(tipo Programa) (horario 0000) (edadRecomendada 18) (sinopsis "Actualidad, entrevistas")  				 (empleadosPrograma "Risto Mejide") (tematica Debate) 	(explorado NO))
    (Programa (idPrograma Poker) 					(nombre "Campeonato de Poker") 				(tipo Programa) (horario 0090) (edadRecomendada 18) (sinopsis "Torneo europeo de poker") 				 (tematica Juego) 			(explorado NO))
    (Programa (idPrograma GaningCasino)				(nombre "GanIng Casino") 					(tipo Programa) (horario 0240) (edadRecomendada 18) (sinopsis "Juegos de casino") 						 (tematica Juego) 			(explorado NO))
    (Programa (idPrograma Noticias3_mañana) 		(nombre "Las noticias de la mañana") 		(tipo Programa) (horario 0360) (edadRecomendada 7)  (sinopsis "Actualidad, noticias Antena3") 			 (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma Arguiñano) 				(nombre "Karlos Arguiñano en tu cocina") 	(tipo Programa) (horario 0540) (edadRecomendada 7)  (sinopsis "Comida sana y equilibrada")  			 (empleadosPrograma "Karlos Arguiñano") (tematica Gastronomia) (explorado NO))
    (Programa (idPrograma RuletaSuerte) 			(nombre "La ruleta de la suerte") 			(tipo Programa) (horario 0720) (edadRecomendada 7)  (sinopsis "Concurso")  								 (tematica Entretenimiento) (explorado NO))
    (Programa (idPrograma Simpsons) 				(nombre "Los simpsons") 					(tipo Serie)    (horario 0840) (edadRecomendada 13) (sinopsis "Dibujos animados") 						 (tematica Animacion) 		(explorado NO))
    (Programa (idPrograma PuenteViejo) 				(nombre "El secreto de Puente Viejo")		(tipo Serie)	(horario 1050) (edadRecomendada 7)  (sinopsis "Telenovela")  							 (tematica Epoca) 			(explorado NO))
    (Programa (idPrograma AmarSiempre) 				(nombre "Amar es para siempre") 			(tipo Serie) 	(horario 0960) (edadRecomendada 7)  (sinopsis "Telenovela") 							 (tematica Epoca) 			(explorado NO))
    (Programa (idPrograma AhoraCaigo) 				(nombre "Ahora Caigo") 					 	(tipo Programa) (horario 1170) (edadRecomendada 7)  (sinopsis "Concurso")  							 	 (empleadosPrograma "Arturo Valls") (tematica Entretenimiento) (explorado NO))
    (Programa (idPrograma Noticias3_noche) 			(nombre "Noticias Antena3 Noche") 			(tipo Programa) (horario 1230) (edadRecomendada 7)  (sinopsis "Actualidad, noticias Antena3") 		 	 (tematica Noticias) 		(explorado NO))
	(Programa (idPrograma Peliculon) 				(nombre "Transporter 3") 					(tipo Pelicula) (horario 1350) (edadRecomendada 13) (sinopsis "Pelicula") 							 	 (tematica Accion)   		(explorado NO))
  	(Programa (idPrograma Hormiguero) 				(nombre "El Hormiguero 3.0") 				(tipo Programa) (horario 1290) (edadRecomendada 13) (sinopsis "Pelicula") 								 (tematica Entretenimiento) (explorado NO))
    ;La1
    (Programa (idPrograma Noticias1) 				(nombre "Telediario 1") 					(tipo Programa) (horario 0900) (edadRecomendada 7)  (sinopsis "Actualidad, noticias TVE")  				 (empleadosPrograma "Silvia Barba") (tematica Noticias) (explorado NO))
    (Programa (idPrograma Cine1) 					(nombre "El verano de la verdad (cine)") 	(tipo Pelicula) (horario 0980) (edadRecomendada 7)  (sinopsis "Película") 								 (tematica Romantico) 		(explorado NO))
    (Programa (idPrograma 24h) 						(nombre "La noche en 24h") 					(tipo Programa) (horario 0090) (edadRecomendada 13) (sinopsis "Actualidad, entrevistas, noticias TVE")   (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma TVEmusica) 				(nombre "TVE es música") 					(tipo Programa) (horario 0215) (edadRecomendada 13) (sinopsis "Música, videoclips") 				 	 (tematica Musica) 			(explorado NO))
    (Programa (idPrograma Noticias1_mañana) 		(nombre "Telediario matinal") 				(tipo Programa) (horario 0390) (edadRecomendada 7)  (sinopsis "Actualidad, noticias TVE") 				 (tematica Noticias) 		(explorado NO))       
    (Programa (idPrograma Mañanadela1) 				(nombre "La mañana de la 1") 				(tipo Programa) (horario 0600) (edadRecomendada 13) (sinopsis "Actualidad, noticias económicas TVE") 	 (tematica Economia) 		(explorado NO))
    (Programa (idPrograma TorresCocina) 			(nombre "Torres en la cocina") 				(tipo Programa) (horario 0800) (edadRecomendada 7)  (sinopsis "Comida sana") 							 (tematica Gastronomia) 	(explorado NO))
    (Programa (idPrograma Corazon) 					(nombre "Corazón") 							(tipo Programa) (horario 0840) (edadRecomendada 13) (sinopsis "Cotilleos") 								 (tematica Corazon) 		(explorado NO))
    (Programa (idPrograma Acacias38) 				(nombre "Acacias 38")						(tipo Serie)    (horario 0985) (edadRecomendada 13) (sinopsis "Telenovela")								 (tematica Epoca) 			(explorado NO))
    (Programa (idPrograma SeisHermanas) 			(nombre "Seis Hermanas") 					(tipo Serie)    (horario 1040) (edadRecomendada 13) (sinopsis "Telenovela")								 (tematica Epoca) 			(explorado NO))
    (Programa (idPrograma AquiTierra) 				(nombre "Aquí la tierra") 					(tipo Programa) (horario 1180) (edadRecomendada 7)  (sinopsis "Salud, arquitectura, geografía") 		 (tematica Ciencia) 		(explorado NO))
    (Programa (idPrograma Noticias1_noche) 			(nombre "Telediario 2") 					(tipo Programa) (horario 1260) (edadRecomendada 7)  (sinopsis "Actualidad, noticias TVE") 				 (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma ComandoActualidad) 		(nombre "Comando actualidad")				(tipo Programa) (horario 0000) (edadRecomendada 13) (sinopsis "Actualidad") 							 (tematica Politica) 		(explorado NO))
    (Programa (idPrograma Cine1_otro) 				(nombre "Toy Story 2 (cine)") 				(tipo Pelicula) (horario 1320) (edadRecomendada 7)  (sinopsis "Película") 								 (tematica Fantasia) 		(explorado NO))
    ;La2
    (Programa (idPrograma HierroyCromo) 			(nombre "Cachitos de Hierro y Cromo") 		(tipo Programa) (horario 0000) (edadRecomendada 13) (sinopsis "Música") 								 (tematica Musica) 			(explorado NO))
    (Programa (idPrograma TVE2Musica) 				(nombre "TVE es música") 					(tipo Programa) (horario 0230) (edadRecomendada 13) (sinopsis "Música") 								 (tematica Musica) 			(explorado NO))
    (Programa (idPrograma Bubbles) 					(nombre "Documental: Bubbles") 				(tipo Programa) (horario 0360) (edadRecomendada 7)  (sinopsis "Fauna y flora submarina") 				 (tematica Documental) 		(explorado NO))
    (Programa (idPrograma Agrosfera) 				(nombre "Agrosfera") 						(tipo Programa) (horario 0540) (edadRecomendada 7)  (sinopsis "Actualidad agrícola, pesquera y ganadera")(tematica Ciencia) 		(explorado NO))
    (Programa (idPrograma Paraisos) 				(nombre "Documental: Paraísos cercanos") 	(tipo Programa) (horario 0660) (edadRecomendada 7)  (sinopsis "Lugares paradisíacos") 					 (tematica Documental) 		(explorado NO))
    (Programa (idPrograma Cine2) 					(nombre "Milagro en casa (cine)") 			(tipo Pelicula) (horario 0720) (edadRecomendada 13) (sinopsis "Película") 								 (tematica Comedia) 		(explorado NO))
    (Programa (idPrograma ViajeCentro) 				(nombre "Viaje al centro de la tele") 		(tipo Programa) (horario 0840) (edadRecomendada 7)  (sinopsis "Histoia de la tele") 					 (tematica Epoca) 			(explorado NO))
    (Programa (idPrograma SaberGanar) 				(nombre "Saber y ganar") 					(tipo Programa) (horario 0960) (edadRecomendada 7)  (sinopsis "Concurso") 								 (tematica Entretenimiento) (explorado NO))
    (Programa (idPrograma Documentales)				(nombre "Documental: Grandes Documentales") (tipo Programa) (horario 1080) (edadRecomendada 7)  (sinopsis "Documental") 							 (tematica Documental) 		(explorado NO))
    (Programa (idPrograma Documental2) 				(nombre "Documental: Documental 2") 		(tipo Programa) (horario 1170) (edadRecomendada 7)  (sinopsis "Documental") 							 (tematica Documental) 		(explorado NO))
    (Programa (idPrograma Cine2_otro) 				(nombre "El bueno, el feo y el malo (cine)")(tipo Pelicula) (horario 1320) (edadRecomendada 13) (sinopsis "Película") 								 (tematica Epoca) 			(explorado NO))    
    ;Cuatro
    (Programa (idPrograma Cine4) 					(nombre "After party (cine)") 				(tipo Pelicula) (horario 0000) (edadRecomendada 18) (sinopsis "Película") 								 (tematica Drama) 			(explorado NO))
    (Programa (idPrograma PuroCuatro) 				(nombre "Puro Cuatro") 						(tipo Programa) (horario 0195) (edadRecomendada 13) (sinopsis "Videoclips") 							 (tematica Musica) 			(explorado NO))
    (Programa (idPrograma Zapping) 					(nombre "El zapping de surferos") 			(tipo Programa) (horario 0420) (edadRecomendada 7)  (sinopsis "Recopilatorio ocurrencias en televisión") (tematica Entretenimiento) (explorado NO))
    (Programa (idPrograma Billy) 					(nombre "Billy el exterminador") 			(tipo Programa) (horario 0510) (edadRecomendada 13) (sinopsis "Exterminador de bichos") 				 (tematica Ciencia) 		(explorado NO))
    (Programa (idPrograma AlertaCobra) 				(nombre "Alerta Cobra") 					(tipo Serie)    (horario 0570) (edadRecomendada 18) (sinopsis "Serie de policias, acción y persecución") (tematica Accion) 			(explorado NO))
    (Programa (idPrograma MañanasCuatro) 			(nombre "Las mañanas de Cuatro") 			(tipo Programa) (horario 0740) (edadRecomendada 7)  (sinopsis "Actualidad, política y economía") 		 (tematica Economia) 		(explorado NO))
    (Programa (idPrograma Noticias4) 				(nombre "Noticias Cuatro mediodía") 		(tipo Programa) (horario 0840) (edadRecomendada 7)  (sinopsis "Actualidad y noticias Cuatro") 			 (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma Deportes4) 				(nombre "Deportes Cuatro") 					(tipo Programa) (horario 0900) (edadRecomendada 7)  (sinopsis "Deportes,¿qué le pasa a Messi?") 		 (tematica Deporte) 		(explorado NO))
    (Programa (idPrograma Hawai) 					(nombre "Hawai 5.0") 						(tipo Serie)    (horario 0960) (edadRecomendada 13) (sinopsis "Serie policíaca")						 (tematica Accion) 			(explorado NO))
    (Programa (idPrograma ReglasJuego) 				(nombre "Las reglas del juego") 			(tipo Serie)    (horario 1140) (edadRecomendada 13) (sinopsis "Serie de engaños y robos") 				 (tematica Drama) 			(explorado NO))
    (Programa (idPrograma Noticias4_noche) 			(nombre "Noticias Cuatro noche") 			(tipo Programa) (horario 1200) (edadRecomendada 7)  (sinopsis "Actualidad y noticias Cuatro") 			 (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma GymTony) 					(nombre "Gym Tony") 						(tipo Serie)    (horario 1260) (edadRecomendada 13) (sinopsis "Comedia") 								 (tematica Comedia) 		(explorado NO))
    (Programa (idPrograma CSI) 						(nombre "C.S.I. Las vegas") 				(tipo Serie)    (horario 1350) (edadRecomendada 18) (sinopsis "Serie policíaca") 						 (tematica Accion) 			(explorado NO))
	;LaSexta
    (Programa (idPrograma PoliciasAccion) 			(nombre "Policias en acción") 				(tipo Programa) (horario 1320) (edadRecomendada 18) (sinopsis "Intervenciones policiales") 				 (tematica Documental) 		(explorado NO))
    (Programa (idPrograma PokerAdventure) 			(nombre "Poker Caribbean Adventure") 		(tipo Programa) (horario 0120) (edadRecomendada 18) (sinopsis "Poker")									 (tematica Juego) 			(explorado NO))
    (Programa (idPrograma Juega8) 					(nombre "Juega con el 8") 					(tipo Programa) (horario 0180) (edadRecomendada 18) (sinopsis "Billar")									 (tematica Juego) 			(explorado NO))
    (Programa (idPrograma MinMusicales) 			(nombre "Minutos musicales") 				(tipo Programa) (horario 0240) (edadRecomendada 13) (sinopsis "Música, videoclips") 					 (tematica Musica) 			(explorado NO))
    (Programa (idPrograma CrimenImperfectos) 		(nombre "Documental: Crímenes imperfectos") (tipo Programa) (horario 0310) (edadRecomendada 18) (sinopsis "Documental sobre crímenes mal ejecutados")(tematica Documental) 		(explorado NO))
    (Programa (idPrograma RojoVivo) 				(nombre "Al rojo vivo") 					(tipo Programa) (horario 0645) (edadRecomendada 13) (sinopsis "Actualidad, entrevistas, debate") 		 (tematica Politica) 		(explorado NO))
    (Programa (idPrograma Noticias6) 				(nombre "La sexta noticias 1 Edicion") 		(tipo Programa) (horario 0840) (edadRecomendada 7)  (sinopsis "Actualidad y noticias LaSexta") 			 (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma Jugones) 					(nombre "Jugones") 							(tipo Programa) (horario 0900) (edadRecomendada 7)  (sinopsis "Deportes, ¿qué le pasa a Cristiano?") 	 (tematica Deporte) 		(explorado NO))
    (Programa (idPrograma Zapeando) 				(nombre "Zapeando") 						(tipo Programa) (horario 0960) (edadRecomendada 13) (sinopsis "Comedia, actualidad") 					 (tematica Noticias) 		(explorado NO))
    (Programa (idPrograma MasValeTarde) 			(nombre "Más vale tarde") 					(tipo Programa) (horario 1020) (edadRecomendada 13) (sinopsis "Actualidad, entrevistas y debates") 		 (tematica Politica) 		(explorado NO))
    (Programa (idPrograma Noticias6_noche) 			(nombre "La sexta noticias 2 Edicion") 		(tipo Programa) (horario 1260) (edadRecomendada 7)  (sinopsis "Actualidad y noticias LaSexta") 			 (tematica Noticias) 		(explorado NO))
    ;Canal+
    (Programa (idPrograma GuerraMundial2) 			(nombre "2º guerra mundial") 				(tipo Pelicula) (horario 0930) (edadRecomendada 18) (sinopsis "2 Guerra mundial") 						 (tematica Belica) 			(explorado NO))    
    (Programa (idPrograma GuerraMundial) 			(nombre "1º guerra mundial") 				(tipo Pelicula) (horario 0000) (edadRecomendada 18) (sinopsis "1 Guerra mundial") 						 (tematica Belica) 			(explorado NO))
    (Programa (idPrograma Champions1) 				(nombre "Partido: Malmö FF - R.Madrid") 	(tipo Programa) (horario 1245) (edadRecomendada 7)  (sinopsis "Partido champions") 						 (tematica Deporte) 		(explorado NO))
    (Programa (idPrograma Champions2) 				(nombre "Partido: Lyon - Valencia") 		(tipo Programa) (horario 0750) (edadRecomendada 7)  (sinopsis "Partido champions") 						 (tematica Deporte) 		(explorado NO))
    (Programa (idPrograma CineAdultos1) 			(nombre "Ensalada de pepino en ... ")		(tipo Pelicula) (horario 0285) (edadRecomendada 18) (sinopsis "Cine lésbico +18") 						 (tematica Adultos) 		(explorado NO))
    (Programa (idPrograma CineAdultos2) 			(nombre "Fue a por trabajo y ...")			(tipo Pelicula) (horario 0180) (edadRecomendada 18) (sinopsis "Cine XXX +18") 							 (tematica Adultos) 		(explorado NO))
    (Programa (idPrograma Basket) 					(nombre "Final Eurobasket") 				(tipo Programa) (horario 0960) (edadRecomendada 7)  (sinopsis "Final eurobasket")						 (tematica Deporte) 		(explorado NO))
    ;CartoonNetwork
    (Programa (idPrograma Dibujos1) 				(nombre "One piece") 						(tipo Serie)    (horario 1200) (edadRecomendada 7)  (sinopsis "Piratas") 								 (tematica Aventuras) 		(explorado NO))
    (Programa (idPrograma Dibujos2) 				(nombre "Naruto") 							(tipo Serie)    (horario 1020) (edadRecomendada 7)  (sinopsis "Ninjas") 								 (tematica Aventuras) 		(explorado NO))
    (Programa (idPrograma Dibujos3) 				(nombre "Pokemon") 							(tipo Serie)    (horario 0840) (edadRecomendada 7)  (sinopsis "Animalicos con poderes") 				 (tematica Aventuras) 		(explorado NO))
    (Programa (idPrograma Dibujos4) 				(nombre "SinChan") 							(tipo Serie)    (horario 0000) (edadRecomendada 7)  (sinopsis "Culito culito") 							 (tematica Animacion) 		(explorado NO))
    (Programa (idPrograma Dibujos5) 				(nombre "Pocoyo") 							(tipo Serie)    (horario 0600) (edadRecomendada 3)  (sinopsis "Pocoyo y sus amigos") 					 (tematica Infantil) 		(explorado NO))
    (Programa (idPrograma Dibujos6) 				(nombre "Caillou") 							(tipo Serie)    (horario 0480) (edadRecomendada 3)  (sinopsis "Caillou y sus amigos") 					 (tematica Infantil) 		(explorado NO))
    (Programa (idPrograma Dibujos7) 				(nombre "Dora la exploradora")				(tipo Serie)    (horario 0690) (edadRecomendada 3)  (sinopsis "Dora la exploradora y sus amigos") 		 (tematica Infantil) 		(explorado NO))    
    (Programa (idPrograma Dibujos8) 				(nombre "Hora de aventuras") 				(tipo Serie)    (horario 0900) (edadRecomendada 7)  (sinopsis "Hora de aventuras") 						 (tematica Aventuras) 		(explorado NO))    
)
;; ProgramasRadio (Añadir programas radio) ;;
(deffacts ProgramasRadio
	(Programa (idPrograma ElLarguero) 				(nombre "El Larguero") 						(tipo Programa) (horario 0930) (edadRecomendada 7)  (sinopsis "Tu programa de deportes") 				 (tematica Deporte) 		(explorado NO))    
	(Programa (idPrograma ElTecno) 					(nombre "Tecnologia") 						(tipo Programa) (horario 0830) (edadRecomendada 7)  (sinopsis "Tecnologia") 							 (tematica Tecnologia) 		(explorado NO))
)


;; Entrada de datos ;;
(defrule nuevoUsuario
	(declare (no-loop TRUE) (salience 3000))  
     =>    
    (printout t "Introduce el identificador del usuario" crlf)
    (bind ?idUsuario (read))
    (printout t "Introduce el nombre completo del usuario" crlf)
    (bind ?nombreUsuario (readline))
    (printout t "Introduce la edad del usuario" crlf)
    (bind ?edadUsuario (read))
    (printout t "Introduce el estado civil del usuario [CASADO/SOLTERO]" crlf)
    (bind ?estadoCivil (read))
    (printout t "¿Tiene el usuario hijos menores? [SI/NO]" crlf)
    (bind ?hijos (read))
    (printout t "¿En qué sector trabaja el usuario? (En mayusculas) Ej: HOSTELERIA, EDUCACION, ..." crlf )
    (bind ?trabajo (read))
    (printout t "Introduce el pais del usuario" crlf)
    (bind ?pais (read))
    (printout t "Introduce la comunidad autonoma del usuario" crlf)
    (bind ?cautonoma (readline))
    (printout t "Introduce la hora a la que el usuario empieza a ver la television (en minutos, ej: 1020=17:00)" crlf)
    (bind ?horarioVision (read))
    (printout t "Introduce el tiempo que quiere consumir el usuario visualizando contenido (en minutos)" crlf)
    (bind ?tiempoVision (read))
    (printout t "¿Posee algún dispositivo que le permita visualizar contenido online? [SI/NO]" crlf)
    (bind ?online (read))
    (printout t "¿Posee television el usuario? [SI/NO]" crlf)
    (bind ?television (read))
    (printout t "¿Posee radio el usuario? [SI/NO]" crlf)
    (bind ?radio (read))
    (printout t "¿El usuario posee television de pago? [SI/NO]" crlf)
    (bind ?pago (read))
    (printout t "¿Tiene hobbies el usuario?" crlf)
    (assert (masHobbies (read)))
    (assert (testPerson NO))
    (assert (Usuario (idUsuario ?idUsuario) (nombre ?nombreUsuario) (edad ?edadUsuario) (estadoCivil ?estadoCivil) (hijos ?hijos) (trabajo ?trabajo) (pais ?pais) (cautonoma ?cautonoma) (horarioVision ?horarioVision) (tiempoVision ?tiempoVision) (dispositivos ?online ?television ?radio) (disponiblePago ?pago)))
    
)

(defrule añadirHobbies
    (declare (salience 3000)) 
    ?f1 <- (masHobbies SI)
    ?usuario <- (Usuario (hobbies $?a))
    =>
    (printout t "Introduce el hobbie (en mayusculas):" crlf)
    (bind ?hobbie (read))
    (printout t "Tiene el usuario algún hobbie más? [SI/NO]" crlf)
    (retract ?f1)
    (assert (masHobbies (read)))
    (modify ?usuario (hobbies $?a ?hobbie))   
)
;; Test de personalidad ;;

(deffunction testPersonalidad()
    (printout t crlf " ---- Personalida desconocida, por favor, rellena el siguiente test ----" crlf crlf)
    (printout t "¿Te sientes mejor por la mañana? [SI/NO]")
    (bind ?p1 (read))
    (printout t "¿Caminas rápido por la calle? [SI/NO]")
    (bind ?p2 (read))
    (printout t "¿Das pasos largos al caminar? [SI/NO]")
    (bind ?p3 (read))
    (printout t "¿Entras ruidosamente en reuniones sociales? [SI/NO]")
    (bind ?p4 (read))
    (printout t "¿Te ries cuando algo te divierte? [SI/NO]")
    (bind ?p5 (read))
    (printout t "¿Sueñas en ocasiones que estás volando? [SI/NO]")
    (bind ?p6 (read))
    (bind ?countSI 0)
    (bind ?countNO 0)
    (if (eq ?p1 SI) then (bind ?countSI (+ ?countSI 1)) else (bind ?countNO (+ ?countNO 1)))
    (if (eq ?p2 SI) then (bind ?countSI (+ ?countSI 1)) else (bind ?countNO (+ ?countNO 1)))
    (if (eq ?p3 SI) then (bind ?countSI (+ ?countSI 1)) else (bind ?countNO (+ ?countNO 1)))
    (if (eq ?p4 SI) then (bind ?countSI (+ ?countSI 1)) else (bind ?countNO (+ ?countNO 1)))
    (if (eq ?p5 SI) then (bind ?countSI (+ ?countSI 1)) else (bind ?countNO (+ ?countNO 1)))
    (if (eq ?p6 SI) then (bind ?countSI (+ ?countSI 1)) else (bind ?countNO (+ ?countNO 1)))
    (if (>  ?countSI ?countNO)  then (return EXTRO))
    (if (<  ?countSI ?countNO)  then (return INTRO))
    (if (eq ?countSI ?countNO)  then (return MED))  
)

(defrule computarPersonalidad
    (declare (no-loop TRUE) (salience 2000))
    ?f1 <- (testPerson NO)
	?usuario <- (Usuario (idUsuario ?) (personalidad nil))
    => 
    (retract ?f1)
    (bind ?personalidad (testPersonalidad))
    (modify ?usuario (personalidad ?personalidad))
    
)

;; Reglas Preferencias ;;

;; Preferencias edad ;;
(defrule preferenciasEdad07
    (declare (no-loop TRUE) (salience 1000))
	 ?usuario <- (Usuario (idUsuario ?) (edad ?edad) (preferencias $?prefUsuario))
    (test (and (>= ?edad 0) (<= ?edad 7)))
    =>
    (modify ?usuario (preferencias $?prefUsuario Infantil Animadas Aventura Fantasia))
)

(defrule preferenciasEdad1216
    (declare (no-loop TRUE) (salience 1000))
	 ?usuario <- (Usuario (idUsuario ?) (edad ?edad) (preferencias $?prefUsuario))
    (test (and (>= ?edad 12) (<= ?edad 16)))
    =>
    (modify ?usuario (preferencias $?prefUsuario Adolescente))
)

(defrule preferenciasEdad50
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (edad ?edad) (preferencias $?prefUsuario))
    (test (>= ?edad 50))
    =>
    (modify ?usuario (preferencias $?prefUsuario Epoca Oeste Juego))
)

;; Preferencias estado civil ;;
(defrule preferenciasSoltero
	(declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (estadoCivil SOLTERO) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (estadoCivil nil) (preferencias $?prefUsuario Corazon Drama Adultos Gastronomia Juego))
)

(defrule preferenciasCasado
	(declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (estadoCivil CASADO) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (estadoCivil nil) (preferencias $?prefUsuario Romantico Corazon Drama))
)

;; Preferencias por hijos ;;
(defrule preferenciasHijos
	(declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (hijos SI) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (hijos nil) (preferencias $?prefUsuario Infantil Aventuras Fantasia Animacion))  
)

(defrule preferenciasDesempleado
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (trabajo nil) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (trabajo FIN) (preferencias $?prefUsuario Empleo Debate Noticias))  
)

(defrule preferenciasTurimo
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (trabajo TURISMO) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (trabajo FIN) (preferencias $?prefUsuario Documental Belica))  
)
(defrule preferenciasHosteleria
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (trabajo HOSTELERIA) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (trabajo FIN) (preferencias $?prefUsuario Gastronomia))  
)
(defrule preferenciasEducacion
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (trabajo EDUCACION) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (trabajo FIN) (preferencias $?prefUsuario Ciencia Tecnologia Noticias Debate))  
)
(defrule preferenciasCienciasSociales
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (trabajo CSOCIALES) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (trabajo FIN) (preferencias $?prefUsuario Economia Politica Debate Noticias))  
)

;; Preferencias por hobby ;;
(defrule preferenciasDeporte
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (hobbies $?a DEPORTE $?b) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (hobbies $?a $?b)(preferencias $?prefUsuario Deporte))  
)

(defrule preferenciasMusica
	(declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (hobbies $?a MUSICA $?b) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (hobbies $?a $?b) (preferencias $?prefUsuario Musica))      
)
(defrule preferenciasVideojuegos
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (hobbies $?a VIDEOJUEGOS $?b) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (hobbies $?a $?b) (preferencias $?prefUsuario Entretenimiento Accion Aventuras Ciencia Tecnologia))  
)

;; Preferencias personalidad ;;

(defrule preferenciasExtrovertido
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (personalidad EXTRO) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (personalidad EXTROVERTIDO) (preferencias $?prefUsuario Entretenimiento Comedia Musica))  
)

(defrule preferenciasIntrovertido
    (declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (personalidad INTRO) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (personalidad INTROVERTIDO) (preferencias $?prefUsuario Drama Belicas))  
)

(defrule preferenciasMedia
	(declare (no-loop TRUE) (salience 1000))
	?usuario <- (Usuario (idUsuario ?) (personalidad MED) (preferencias $?prefUsuario))
    =>
    (modify ?usuario (personalidad MEDIA) (preferencias $?prefUsuario Drama Entretenimiento)) 
)

;; Reglas Filtrado ;;

(defrule programasPorEdad
    ?usuario <- (Usuario (idUsuario ?) (edad ?edadUsuario) (programasRecomendados $?programasRecomendados))
	?programaRecomendado <- (Programa (idPrograma ?idPrograma) (edadRecomendada ?edadRecomendada) (explorado NO))
    (test (>= ?edadUsuario ?edadRecomendada))
    =>
    (modify ?programaRecomendado (explorado SI))
    (modify ?usuario (programasRecomendados $?programasRecomendados ?idPrograma))
)


(defrule programasPorCAutonoma
	?usuario <- (Usuario (idUsuario ?) (programasRecomendados $?a ?programaRecomendado $?b) (cautonoma ?autonomiaUsuario))
	(not (exists (Cadena (alcance ?autonomiaUsuario) (programasCadena $? ?programaRecomendado $?))))
    (not (exists (Cadena (alcance Nacional) (programasCadena $? ?programaRecomendado $?)))) 
  
   =>
   (modify ?usuario (programasRecomendados $?a $?b))
)

; Añadir parámetro para diferencia de horas entre horario vision y horario programa ;
(defrule programasPorHorario
    ?usuario <- (Usuario (idUsuario ?) (programasRecomendados $?a ?programaRecomendado $?b) (horarioVision ?horarioVision) (tiempoVision ?tiempoVision))
    (Programa (idPrograma ?programaRecomendado) (horario ?horarioPrograma))
    (test (or (and (< ?horarioPrograma ?horarioVision) (> ?horarioPrograma (mod (+ ?horarioVision ?tiempoVision) 1439))) (or (and(< ?horarioPrograma ?horarioVision)(< ?horarioPrograma (+ ?horarioVision ?tiempoVision))) (and(> ?horarioPrograma ?horarioVision)(> ?horarioPrograma (+ ?horarioVision ?tiempoVision)))) ))
    =>     
    (modify ?usuario (programasRecomendados $?a $?b))
)

(defrule programasPorDispositivoTV
	?usuario <- (Usuario (idUsuario ?) (programasRecomendados $?a ?programaRecomendado $?b) (dispositivos ? NO ?))
	(CadenaTV (programasCadena $? ?programaRecomendado $?) (online NO)) 
   =>
   
    (modify ?usuario (programasRecomendados $?a $?b))
)

(defrule programasPorDispositivoOnlineTV
	?usuario <- (Usuario (idUsuario ?) (programasRecomendados $?a ?programaRecomendado $?b) (dispositivos NO NO ?))
	(CadenaTV (programasCadena $? ?programaRecomendado $?) (online SI)) 
    =>
    (modify ?usuario (programasRecomendados $?a $?b))
)

(defrule programasPorDispositivoOnlineRD
	?usuario <- (Usuario (idUsuario ?) (programasRecomendados $?a ?programaRecomendado $?b) (dispositivos NO ? NO))
	(CadenaRadio (programasCadena $? ?programaRecomendado $?) (online SI)) 
    =>       
    (modify ?usuario (programasRecomendados $?a $?b))
)


(defrule programasPorDispositivoRD
	?usuario <- (Usuario (idUsuario ?) (programasRecomendados $?a ?programaRecomendado $?b) (dispositivos ? ? NO))
	(CadenaRadio (programasCadena $? ?programaRecomendado $?) (online NO)) 
    =>    
    (modify ?usuario (programasRecomendados $?a $?b))
)

(defrule programasTVPorPago
    ?usuario <- (Usuario (idUsuario ?) (programasRecomendados $?a ?programaRecomendado $?b) (disponiblePago ?pagoUsuario))
    (CadenaTV (programasCadena $? ?programaRecomendado $?)  (pago ?pagoCadena))
    (test (and (eq ?pagoUsuario NO) (eq ?pagoCadena SI)))
    =>
    (modify ?usuario (programasRecomendados $?a $?b))
)

(defrule programasPorPreferencia
    (declare (salience -10))
	?usuario <- (Usuario (idUsuario ?) (programasRecomendados $?a ?programaRecomendado $?b) (preferencias $?prefUsuario))
    (Programa (idPrograma ?programaRecomendado) (tematica ?tematicaPrograma))
    (test (not (member$ ?tematicaPrograma $?prefUsuario)))
    =>
    (modify ?usuario (programasRecomendados $?a $?b))   
)

(defrule programasRecomendadosTVNoOnline
	(declare (salience -100))
   	(Usuario (nombre ?nombreUsuario) (programasRecomendados $? ?programaRecomendado $?))
    (Programa (idPrograma ?programaRecomendado) (nombre ?nombrePrograma) (horario ?horarioPrograma) (tipo ?tipoPrograma) (tematica ?tematicaPrograma) (sinopsis ?sinopsisPrograma))
    (CadenaTV (programasCadena $? ?programaRecomendado $?) (online NO) (nombre ?nombreCadena) (pago ?pagoCadena))
    =>
    (printout t "**********************************" crlf)  
    (printout t "Recomendacion " ?tipoPrograma " para " ?nombreUsuario " : " ?nombrePrograma crlf)
    (printout t "**********************************" crlf)    
    (printout t "Tematica: " ?tematicaPrograma crlf)
    (printout t "Sinopsis: " ?sinopsisPrograma crlf)
    (printout t "Horario: "  (round (/ ?horarioPrograma 60)) ":" (mod ?horarioPrograma 60) crlf)
    (printout t "Cadena: " ?nombreCadena crlf)
    (printout t "Pago: " ?pagoCadena crlf crlf) 
    (printout t "**********************************" crlf crlf)   
    
)

(defrule programasRecomendadosRadioNoOnline
	(declare (salience -100))
   	(Usuario (nombre ?nombreUsuario) (programasRecomendados $? ?programaRecomendado $?))
    (Programa (idPrograma ?programaRecomendado) (nombre ?nombrePrograma) (horario ?horarioPrograma) (tipo ?tipoPrograma) (tematica ?tematicaPrograma) (sinopsis ?sinopsisPrograma))
    (CadenaRadio (programasCadena $? ?programaRecomendado $?) (online NO) (nombre ?nombreCadena) (frecuencia ?frecuenciaCadena))
    =>
    (printout t "**********************************" crlf)  
    (printout t "Recomendacion " ?tipoPrograma " para " ?nombreUsuario " : " ?nombrePrograma crlf)
    (printout t "**********************************" crlf)    
    (printout t "Tematica: " ?tematicaPrograma crlf)
    (printout t "Sinopsis: " ?sinopsisPrograma crlf)
    (printout t "Horario: "  (round (/ ?horarioPrograma 60)) ":" (mod ?horarioPrograma 60) crlf)
    (printout t "Cadena: " ?nombreCadena crlf)
    (printout t "Frecuencia: " ?frecuenciaCadena crlf crlf) 
    (printout t "**********************************" crlf crlf)    
)

(defrule programasRecomendadosTVOnline
	(declare (salience -100))
   	(Usuario (nombre ?nombreUsuario) (programasRecomendados $? ?programaRecomendado $?))
    (Programa (idPrograma ?programaRecomendado) (nombre ?nombrePrograma) (horario ?horarioPrograma) (tipo ?tipoPrograma) (tematica ?tematicaPrograma) (sinopsis ?sinopsisPrograma))
    (CadenaTV (programasCadena $? ?programaRecomendado $?) (online SI) (rutaDirecto ?rutaOnline) (nombre ?nombreCadena) (pago ?pagoCadena))
    =>
    (printout t "**********************************" crlf)  
    (printout t "Recomendacion " ?tipoPrograma " para " ?nombreUsuario " : " ?nombrePrograma crlf)
    (printout t "**********************************" crlf) 
    (printout t "Tematica: " ?tematicaPrograma crlf)
    (printout t "Sinopsis: " ?sinopsisPrograma crlf)
    (printout t "Horario: "  (round (/ ?horarioPrograma 60)) ":" (mod ?horarioPrograma 60) crlf)
    (printout t "Cadena: " ?nombreCadena crlf)
    (printout t "Pago: " ?pagoCadena crlf) 
    (printout t "Es posible seguir online en: " ?rutaOnline crlf crlf)
    (printout t "**********************************" crlf crlf)   
)

(defrule programasRecomendadosRadioOnline
	(declare (salience -100))
   	(Usuario (nombre ?nombreUsuario) (programasRecomendados $? ?programaRecomendado $?))
    (Programa (idPrograma ?programaRecomendado) (nombre ?nombrePrograma) (horario ?horarioPrograma) (tipo ?tipoPrograma) (tematica ?tematicaPrograma) (sinopsis ?sinopsisPrograma))
    (CadenaRadio (programasCadena $? ?programaRecomendado $?) (online SI) (rutaDirecto ?rutaOnline) (frecuencia ?frecuenciaCadena) (nombre ?nombreCadena) )
    =>
    (printout t "**********************************" crlf)  
    (printout t "Recomendacion " ?tipoPrograma " para " ?nombreUsuario " : " ?nombrePrograma crlf)
    (printout t "**********************************" crlf)   
    (printout t "Tematica: " ?tematicaPrograma crlf)
    (printout t "Sinopsis: " ?sinopsisPrograma crlf)
    (printout t "Horario: "  (round (/ ?horarioPrograma 60)) ":" (mod ?horarioPrograma 60) crlf)
    (printout t "Cadena: " ?nombreCadena crlf)
    (printout t "Frecuencia: " ?frecuenciaCadena crlf) 
    (printout t "Es posible seguir online en: " ?rutaOnline crlf crlf)
    (printout t "**********************************" crlf crlf)     
)

(reset)
(run)
;(facts)