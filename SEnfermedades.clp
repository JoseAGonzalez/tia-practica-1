(deftemplate Enfermedad
    (slot idEnfermedad (type symbol))
    (slot nombreEnfermedad (type string))
)

(deftemplate Sintoma
    (slot idSintoma (type symbol))
    (slot nombre (type string))
)

(deftemplate Usuario
    (slot idUsuario (type symbol))
    (slot nombreUsuario (type string))
    (slot edadUsuario (type integer))
    (slot gripe (type integer) (default 0))
    (slot hepatitis (type integer) (default 0))
    (slot anemia (type integer) (default 0))
    (slot tuberculosis (type integer) (default 0))
    (slot malaria (type integer) (default 0))
    (slot rubeola (type integer) (default 0))
    (multislot sintomas (type symbol))
    (multislot enfermedades (type symbol))
    (slot enfermedad (type symbol))
)

;(deffacts Usuarios
;	(Usuario (idUsuario Jogonba) (nombreUsuario "José Ángel González Barba") (edadUsuario 21) (sintomas ictericia fiebre tos cansancio dolorCabeza dolorMuscular faltaApetito nauseas diarrea apatia))
;)

(deffacts Sintomas
    (Sintoma (idSintoma tos) (nombre "Tos"))
    (Sintoma (idSintoma cansancio) (nombre "Cansancio"))
    (Sintoma (idSintoma dolorCabeza) (nombre "Dolor de cabeza"))
    (Sintoma (idSintoma dolorMuscular) (nombre "Dolores musculares"))
    (Sintoma (idSintoma faltaApetito) (nombre "Falta de apetito"))
    (Sintoma (idSintoma nauseas) (nombre "Nauseas"))
    (Sintoma (idSintoma diarrea) (nombre "Diarrea"))
    (Sintoma (idSintoma ictericia) (nombre "Ictericia"))
    (Sintoma (idSintoma apatia) (nombre "Apatía"))
    (Sintoma (idSintoma pielPalida) (nombre "Piel pálida"))
    (Sintoma (idSintoma escalofrios) (nombre "Escalofríos"))
    (Sintoma (idSintoma perdidaPeso) (nombre "Pérdida de peso"))
    (Sintoma (idSintoma gangliosInflamados) (nombre "Ganglios linfaticos inflamados"))
    (Sintoma (idSintoma erupcionCutanea) (nombre "Erupción cutánea"))
    (Sintoma (idSintoma fiebre) (nombre "Fiebre"))
    (Sintoma (idSintoma secrecionNasal) (nombre "Secreción nasal"))
)

(deffacts Enfermedades
	(Enfermedad (idEnfermedad gripe) (nombreEnfermedad "Gripe"))
    (Enfermedad (idEnfermedad hepatitis) (nombreEnfermedad "Hepatitis"))
    (Enfermedad (idEnfermedad anemia) (nombreEnfermedad "Anemia"))
    (Enfermedad (idEnfermedad tuberculosis) (nombreEnfermedad "Tuberculosis"))
    (Enfermedad (idEnfermedad malaria) (nombreEnfermedad "Malaria"))
    (Enfermedad (idEnfermedad rubeola) (nombreEnfermedad "Rubéola"))
)

;; Parte 0, entrada de datos ;;

(defrule nuevoPaciente
    (declare (no-loop TRUE) (salience 1000))
	=>    
    (printout t "Introduce el identificador del paciente" crlf)
    (bind ?idPaciente (read))
    (printout t "Introduce el nombre completo del paciente" crlf)
    (bind ?nombrePaciente (readline))
    (printout t "Introduce la edad del paciente" crlf)
    (bind ?edadPaciente (read))
    (printout t "Sufre algún síntoma el paciente? [SI/NO]" crlf)
    (assert (masSintomas (read)))
    (assert (Usuario (idUsuario ?idPaciente) (nombreUsuario ?nombrePaciente) (edadUsuario ?edadPaciente)))
)

(defrule añadirSintomas
	(declare (salience 1000))
    ?f1 <- (masSintomas SI)
    ?usuario <- (Usuario (sintomas $?a))
    =>
    (printout t "Introduce el sintoma" crlf)
    (bind ?sintoma (read))
    (printout t "Sufre el paciente algún síntoma más? [SI/NO]" crlf)
    (retract ?f1)
    (assert (masSintomas (read)))
    (modify ?usuario (sintomas $?a ?sintoma))
)
;; 1º parte, ver todas las enfermedades posibles con los sintomas conocidos ;;

; Gripe ;
(defrule gripe1
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c tos $?d fiebre $?e) (enfermedades $?a) )
    (test (not (member$ gripe $?a)))
    =>
    (modify ?usuario (enfermedades $?a gripe))
)
(defrule gripe2
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c cansancio $?d dolorCabeza $?e) (enfermedades $?a))
    (test (not (member$ gripe $?a)))
    =>
    (modify ?usuario (enfermedades $?a gripe))
)
(defrule gripe3
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c dolorMuscular $?d faltaApetito $?e) (enfermedades $?a))
    (test (not (member$ gripe $?a)))
    =>
    (modify ?usuario (enfermedades $?a gripe))
)

; Hepatitis ;
(defrule hepatitis1
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c nauseas $?d diarrea $?e) (enfermedades $?a))
    (test (not (member$ hepatitis $?a)))
    =>
    (modify ?usuario (enfermedades $?a hepatitis))
)
(defrule hepatitis2
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c ictericia $?d) (enfermedades $?a))
    (test (not (member$ hepatitis $?a)))
    =>
    (modify ?usuario (enfermedades $?a hepatitis))
)
(defrule hepatitis3
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c cansancio $?d faltaApetito $?e fiebre $?f nauseas $?g) (enfermedades $?a))
    (test (not (member$ hepatitis $?a)))
    =>
    (modify ?usuario (enfermedades $?a hepatitis))
)

; Anemia ;
(defrule anemia1
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c cansancio $?d apatia $?e) (enfermedades $?a))
    (test (not (member$ anemia $?a)))
    =>
    (modify ?usuario (enfermedades $?a anemia))
)
(defrule anemia2
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c nauseas $?d) (enfermedades $?a))
    (test (not (member$ anemia $?a)))
    =>
    (modify ?usuario (enfermedades $?a anemia))
)
(defrule anemia3
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c cansancio $?d dolorCabeza $?e pielPalida $?f) (enfermedades $?a))
    (test (not (member$ anemia $?a)))
    =>
    (modify ?usuario (enfermedades $?a anemia))
)

; Tuberculosis ;
(defrule tuberculosis1
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c tos $?d fiebre $?e escalofrios $?f) (enfermedades $?a))
    (test (not (member$ tuberculosis $?a)))
    =>
    (modify ?usuario (enfermedades $?a tuberculosis))
)
(defrule tuberculosis2
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c cansancio $?d perdidaPeso $?e tos $?f) (enfermedades $?a))
    (test (not (member$ tuberculosis $?a)))
    =>
    (modify ?usuario (enfermedades $?a tuberculosis))
)
(defrule tuberculosis3
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c escalofrios $?d tos $?e perdidaApetito $?f) (enfermedades $?a))
    (test (not (member$ tuberculosis $?a)))
    =>
    (modify ?usuario (enfermedades $?a tuberculosis))
)

; Malaria ;
(defrule malaria1
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c escalofrios $?d ictericia $?e) (enfermedades $?a))
    (test (not (member$ malaria $?a)))
    =>
    (modify ?usuario (enfermedades $?a malaria))
)
(defrule malaria2
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c fiebre $?d diarrea $?e) (enfermedades $?a))
    (test (not (member$ malaria $?a)))
    =>
    (modify ?usuario (enfermedades $?a malaria))
)
(defrule malaria3
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c fiebre $?d escalofrios $?e dolorCabeza $?f) (enfermedades $?a))
    (test (not (member$ malaria $?a)))
    =>
    (modify ?usuario (enfermedades $?a malaria))
)

; Rubéola ;

(defrule rubeola1
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c dolorCabeza $?d gangliosInflamados $?e) (enfermedades $?a))
    (test (not (member$ rubeola $?a)))
    =>
    (modify ?usuario (enfermedades $?a rubeola))
)
(defrule rubeola2
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c erupcionCutanea $?d fiebre $?e tos $?f secrecionNasal $?g) (enfermedades $?a))
    (test (not (member$ rubeola $?a)))
    =>
    (modify ?usuario (enfermedades $?a rubeola))
)
(defrule rubeola3
    (declare (no-loop TRUE))
    ?usuario <- (Usuario (sintomas $?c dolorCabeza $?d perdidaApetito $?e secrecionNasal $?f) (enfermedades $?a))
    (test (not (member$ rubeola $?a)))
    =>
    (modify ?usuario (enfermedades $?a rubeola) )
)

;; 2º parte, detectar de las enfermedades posibles, aquella de la que el paciente tiene más síntomas conocidos  (no se pregunta por más síntomas, aunque se podría hacer en las funciones) ;;

(deffunction sintomasGripe($?sintomas)
    (bind ?countSintomas 0)
    (if (member$ tos $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ cansancio $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ dolorCabeza $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ dolorMuscular $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ faltaApetito $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    ;; Se podría preguntar aquí por más síntomas e incrementar el contador ... ;;
    (return ?countSintomas)
)

(defrule enfermedadGripe
    (declare (no-loop TRUE) (salience -500))
    ?usuario <- (Usuario (enfermedades $?a gripe $?b) (sintomas $?sintomas) (gripe 0))
    =>
    (modify ?usuario (gripe  (sintomasGripe $?sintomas)) )
)

(deffunction sintomasHepatitis($?sintomas)
    (bind ?countSintomas 0)
    (if (member$ nauseas $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ diarrea $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ ictericia $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ cansancio $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ faltaApetito $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ fiebre $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    ;; Se podría preguntar aquí por más síntomas e incrementar el contador ... ;;
    (return ?countSintomas)
)

(defrule enfermedadHepatitis
    (declare (no-loop TRUE) (salience -500))
    ?usuario <- (Usuario (enfermedades $?a hepatitis $?b) (sintomas $?sintomas) (hepatitis 0))
    =>
    (modify ?usuario (hepatitis  (sintomasHepatitis $?sintomas)) )
)

(deffunction sintomasAnemia($?sintomas)
    (bind ?countSintomas 0)
    (if (member$ cansancio $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ apatia $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ nauseas $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ dolorCabeza $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ pielPalida $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    ;; Se podría preguntar aquí por más síntomas e incrementar el contador ... ;;
    (return ?countSintomas)
)

(defrule enfermedadAnemia
    (declare (no-loop TRUE) (salience -500))
    ?usuario <- (Usuario (enfermedades $?a anemia $?b) (sintomas $?sintomas) (anemia 0))
    =>
    (modify ?usuario (anemia  (sintomasAnemia $?sintomas)) )
)

(deffunction sintomasTuberculosis($?sintomas)
    (bind ?countSintomas 0)
    (if (member$ tos $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ fiebre $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ escalofrios $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ cansancio $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ perdidaPeso $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ faltaApetito $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    ;; Se podría preguntar aquí por más síntomas e incrementar el contador ... ;;
    (return ?countSintomas)
)

(defrule enfermedadTuberculosis
    (declare (no-loop TRUE) (salience -500))
    ?usuario <- (Usuario (enfermedades $?a tuberculosis $?b) (sintomas $?sintomas) (tuberculosis 0))
    =>
    (modify ?usuario (tuberculosis  (sintomasTuberculosis $?sintomas)) )
)


(deffunction sintomasMalaria($?sintomas)
    (bind ?countSintomas 0)
    (if (member$ escalofrios $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ ictericia $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ fiebre $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ diarrea $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ escalofrios $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ dolorCabeza $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    ;; Se podría preguntar aquí por más síntomas e incrementar el contador ... ;;
    (return ?countSintomas)
)

(defrule enfermedadMalaria
    (declare (no-loop TRUE) (salience -500))
    ?usuario <- (Usuario (enfermedades $?a malaria $?b) (sintomas $?sintomas) (malaria 0))
    =>
    (modify ?usuario (malaria  (sintomasMalaria $?sintomas)) )
)


(deffunction sintomasRubeola($?sintomas)
    (bind ?countSintomas 0)
    (if (member$ dolorCabeza $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ gangliosInflamados $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ erupcionCutanea $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ fiebre $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ tos $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ secrecionNasal $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    (if (member$ perdidaApetito $?sintomas) then (bind ?countSintomas (+ ?countSintomas 1)))
    ;; Se podría preguntar aquí por más síntomas e incrementar el contador ... ;;
    (return ?countSintomas)
)

(defrule enfermedadRubeola
    (declare (no-loop TRUE) (salience -500))
    ?usuario <- (Usuario (enfermedades $?a rubeola $?b) (sintomas $?sintomas) (rubeola 0))
    =>
    (modify ?usuario (rubeola  (sintomasRubeola $?sintomas)) )
)


;; 3º parte, la enfermedad del usuario es aquella que posea un mayor número de síntomas en el paciente ;;

(deffunction maxEnfermedad(?gripe,?hepatitis,?anemia,?tuberculosis,?malaria,?rubeola)
    (if (and (and (and (and (>= ?gripe ?hepatitis) (>= ?gripe ?anemia)) (>= ?gripe ?tuberculosis)) (>= ?gripe ?malaria)) (>= ?gripe ?rubeola)) then (return Gripe))
    (if (and (and (and (and (>= ?hepatitis ?gripe) (>= ?hepatitis ?anemia)) (>= ?hepatitis ?tuberculosis)) (>= ?hepatitis ?malaria)) (>= ?hepatitis ?rubeola)) then (return Hepatitis))
    (if (and (and (and (and (>= ?anemia ?hepatitis) (>= ?anemia ?gripe)) (>= ?anemia ?tuberculosis)) (>= ?anemia ?malaria)) (>= ?anemia ?rubeola)) then (return Anemia))
    (if (and (and (and (and (>= ?tuberculosis ?hepatitis) (>= ?tuberculosis ?anemia)) (>= ?tuberculosis ?gripe)) (>= ?tuberculosis ?malaria)) (>= ?tuberculosis ?rubeola)) then (return Tuberculosis))
    (if (and (and (and (and (>= ?malaria ?hepatitis) (>= ?malaria ?anemia)) (>= ?malaria ?tuberculosis)) (>= ?malaria ?gripe)) (>= ?malaria ?rubeola)) then (return Malaria))
    (if (and (and (and (and (>= ?rubeola ?hepatitis) (>= ?rubeola ?anemia)) (>= ?rubeola ?tuberculosis)) (>= ?rubeola ?malaria)) (>= ?rubeola ?gripe)) then (return Rubeola))
)

(defrule ruleMaxEnfermedad
    (declare (no-loop TRUE) (salience -750))
    ?usuario <- (Usuario (gripe ?gripe) (hepatitis ?hepatitis) (anemia ?anemia) (tuberculosis ?tuberculosis) (malaria ?malaria) (rubeola ?rubeola))
    =>
    (modify ?usuario (enfermedad (maxEnfermedad ?gripe ?hepatitis ?anemia ?tuberculosis ?malaria ?rubeola)))
)

;; 4º parte, mostrar la enfermedad del usuario ;;

(defrule condicionParada
	(declare (salience -1000))
    ?usuario <- (Usuario (nombreUsuario ?n) (edadUsuario ?ed) (enfermedad ?enfermedad) (enfermedades $?enfermedades))
    =>
    (printout t "Usuario " ?n " con " ?ed " años tiene la siguiente enfermedad: " ?enfermedad " de entre las posibles: " $?enfermedades)    
)

(reset)
(run)
(facts)