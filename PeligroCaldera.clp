; Pr�ctica 1 - Caldera - ;
; Alberto Donet Olmeda ;
; Jos� �ngel Gonz�lez Barba ;

(do-backward-chaining presion)
(do-backward-chaining temperatura)


;;;;;;;;;; Preguntas de variables backward-reactivas ;;;;;;;;;;

;; SII no es conocida la presi�n ;;
(defrule medidorPresion
    (need-presion ?)
    =>
    (printout t "[?] Indica la presion actual (Pm) >  " crlf)
    (assert (sensor-presion (read)))    
)

;; SII no es conocida la temperatura ;;
(defrule medidorTemperatura
    (need-temperatura ?)
    =>
    (printout t "[?] Indica la temperatura actual (Tm) >  " crlf)
    (assert (sensor-temperatura (read)))    
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;; Peligro caldera ;;;;;;;;;;


(defrule peligroBB
    (presion BAJA)
    (temperatura BAJA)
    =>
    (assert (peligro BAJO))    
)

(defrule peligroBM
    (presion BAJA)
    (temperatura MEDIA)
    =>
    (assert (peligro BAJO))    
)

(defrule peligroBA
    (presion BAJA)
    (temperatura ALTA)
    =>
    (assert (peligro MEDIO))    
)

(defrule peligroMB
    (presion MEDIA)
    (temperatura BAJA)
    =>
    (assert (peligro MEDIO))    
)

(defrule peligroMM
    (presion MEDIA)
    (temperatura MEDIA)
    =>
    (assert (peligro MEDIO))    
)

(defrule peligroMA
    (presion MEDIA)
    (temperatura ALTA)
    =>
    (assert (peligro MUYALTO))    
)

(defrule peligroAB
    (presion ALTA)
    (temperatura BAJA)
    =>
    (assert (peligro MEDIO))    
)

(defrule peligroAM
    (presion ALTA)
    (temperatura MEDIA)
    =>
    (assert (peligro ALTO))    
)

(defrule peligroAA
    (presion ALTA)
    (temperatura ALTA)
    =>
    (assert (peligro MUYALTO))    
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;; Presi�n cualitativa ;;;;;;;;;;

(defrule presionNegativa
    (declare (salience 1000))
    ?d <- (sensor-presion ?p)
    (test (< ?p 0))
    =>
    (retract ?d)
    (printout t "[?] Indica la presion actual (Sensor) >  " crlf)
    (assert (sensor-presion (read)))  
)
    
(defrule presionBaja
    (declare (salience 1000))
    ?d <- (sensor-presion ?p)
    (test (and (< ?p 10) (> ?p 0)))
    =>
    (retract ?d)
    (assert (presion BAJA))    
)

(defrule presionMedia
    (declare (salience 1000))
    ?d <- (sensor-presion ?p)
    (test (and (>= ?p 10) (< ?p 20)))
    =>
    (retract ?d)
    (assert (presion MEDIA))    
)

(defrule presionAlta
     (declare (salience 1000))
    ?d <- (sensor-presion ?p)
    (test (>= ?p 20))
    =>
    (retract ?d)
    (assert (presion ALTA))      
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;; Temperatura cualitativa ;;;;;;;;;;

(defrule temperaturaNegativa
    (declare (salience 1000))
    ?d <- (sensor-temperatura ?t)
    (test (< ?t 0))
    =>
    (retract ?d)
    (printout t "[?] Indica la temperatura actual (Sensor) >  " crlf)
    (assert (sensor-temperatura (read)))  
)

(defrule temperaturaBaja
    (declare (salience 1000))   
    ?d <- (sensor-temperatura ?t)
    (test (and (< ?t 100) (> ?t 0)))
    =>
    (retract ?d)
    (assert (temperatura BAJA))
)

(defrule temperaturaMedia
    (declare (salience 1000))
    ?d <- (sensor-temperatura ?t)
    (test (and (>= ?t 100) (< ?t 200)))
    =>
    (retract ?d)
    (assert (temperatura MEDIA))       
    
)
(defrule temperaturaAlta
     (declare (salience 1000))
    ?d <- (sensor-temperatura ?t)
    (test (>= ?t 200))
    =>
    (retract ?d)
    (assert (temperatura ALTA))     
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;; Condici�n de parada ;;;;;;;;;;

(defrule configuradoPeligro
    (declare (salience 2000))
    (peligro ?p)
    =>
    (printout t "Peligro de la caldera: " ?p crlf)
    (halt)    
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reset)
;; Si no se le pasan los valores de los sensores, pregunta - hacer lo de valores negativos - ;;

;(assert (sensor-temperatura 301))
;(assert (sensor-presion 50))
(run)